﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using Se.Backend.Services.Common;
using Se.Backend.Entity;
using System.Net;
using System.IO;
namespace Se.Frontend.ConsoleApp
{
    class Program
    {
        private static string _routeDb;
        static void Main(string[] args)
        {
            string rout = Path.Combine(Directory.GetCurrentDirectory(), "DB.xml");
            _routeDb = rout;
            Console.WriteLine(_routeDb);
            Refresh();
            while (true)
            {
                Console.WriteLine("1:Gateway    2:ElectrycityMeter  3:WaterCount    4:ChangeRouteDB     5:Exit");
                Console.WriteLine("Please enter the name or number the element that you wish to create:");
               
                string command = Console.ReadLine();
                try
                {
                    switch (command)
                    {
                        case ("Gateway"):
                        case ("1"):
                            Gateway gateway = CreateGateway();
                            var ser = new Se.Backend.Services.ServiceFactory().CreateAddElementService(_routeDb);
                            Console.WriteLine(ser.AddElement(gateway).ToString());
                            Refresh();

                            break;
                        case ("ElectrycityMeter"):
                        case ("2"):
                            ElectrycityMeter electrycityMeter = CreateElectrycityMeter();
                            var ser1 = new Se.Backend.Services.ServiceFactory().CreateAddElementService(_routeDb);
                            Console.WriteLine(ser1.AddElement(electrycityMeter).ToString());
                            Refresh();
                            break;
                        case ("WaterCounter"):
                        case ("3"):
                            WaterCounter waterCounter = CreateWaterCounter();
                            var ser2 = new Se.Backend.Services.ServiceFactory().CreateAddElementService(_routeDb);
                            Console.WriteLine(ser2.AddElement(waterCounter).ToString());
                            Refresh();
                            break;
                        case ("ChangeRouteDB"):
                        case ("4"):
                            _routeDb = Console.ReadLine();
                            Refresh();
                            break;
                        case ("Exit"):
                        case ("5"):
                            Environment.Exit(0);

                            break;

                        default:
                            break;
                    }
                }
                catch (Exception ex) { Console.WriteLine(ex.Message); }
            }
        }

        private static WaterCounter CreateWaterCounter()
        {
            WaterCounter res = new WaterCounter();

            foreach (var p in res.GetType().GetProperties())
            {
                Console.WriteLine(p.ToString());
                string result = Console.ReadLine();

                var atr = Convert.ChangeType(result, p.PropertyType);
                p.SetValue(res, atr);
            }
            return res;
        }

        private static ElectrycityMeter CreateElectrycityMeter()
        {
            ElectrycityMeter res = new ElectrycityMeter();

            foreach (var p in res.GetType().GetProperties())
            {
                Console.WriteLine(p.ToString());
                string result = Console.ReadLine();

                var atr = Convert.ChangeType(result, p.PropertyType);
                p.SetValue(res, atr);
            }
            return res;
        }

        private static Gateway CreateGateway()
        {
            Gateway res = new Gateway();

            foreach (var p in res.GetType().GetProperties())
            {
                Console.WriteLine(p.ToString());
                string result = Console.ReadLine();
                if (p.PropertyType == typeof(IPAddress))
                {

                    var ip = IPAddress.Parse(result);
                    p.SetValue(res, ip);

                }
                else
                {
                    var atr = Convert.ChangeType(result, p.PropertyType);
                    p.SetValue(res, atr);
                }


            }

     
            return res;
        }

        private static bool Refresh()
        {
            bool res = true;
            
            try
            {
                var ser = new Se.Backend.Services.ServiceFactory().CreateAddElementService(_routeDb);
                foreach (var e in ser.GetElements())
                {
                    Console.WriteLine(  e + "\n");
                }
            }
            catch (Exception e) { res = false; Console.WriteLine(e.Message); }

            return res;

        }
    }
}
