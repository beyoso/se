﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Se.Backend.Entity;
using System.Reflection;
using System.Net;
using System.IO;
using Se.Backend.Services.Common;
namespace Se.Frontend.Wpf
{
    /// <summary>
    /// Lógica de interacción para MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        
        public MainWindow()
        {
            InitializeComponent();
            ComB_item.Items.Add("Gateway");
            ComB_item.Items.Add("ElectrycityMeter");
            ComB_item.Items.Add("WaterCounter");
            ComB_item.SelectedIndex = 1;
            string rout = Path.Combine(Directory.GetCurrentDirectory(), "DB.xml");
            TxtB_route.Text = rout;
            Refresh();
            
            
        }
        private bool Refresh()
        {
            bool res = true;
            TxtB_db.Text = "";
            try
            {
                var ser = new Se.Backend.Services.ServiceFactory().CreateAddElementService(TxtB_route.Text);
                foreach (var e in ser.GetElements()) {
                    TxtB_db.Text += e+"\n";
                        }
            }
            catch (Exception e) { res = false;MessageBox.Show(e.Message); }
            
            return res;

        }
        private void Btn_add_Click(object sender, RoutedEventArgs e)
        {
            try { 
            switch (ComB_item.SelectedValue)
            {
                case ("Gateway"):
                  
                        Gateway gateway = CreateGateway();
                        var ser = new Se.Backend.Services.ServiceFactory().CreateAddElementService(TxtB_route.Text);
                        TxtB_result.Text=  ser.AddElement(gateway).ToString();
                        Refresh();

                        break;
                case ("ElectrycityMeter"):
                    ElectrycityMeter electrycityMeter = CreateElectrycityMeter();
                        var ser1 = new Se.Backend.Services.ServiceFactory().CreateAddElementService(TxtB_route.Text);
                        TxtB_result.Text = ser1.AddElement(electrycityMeter).ToString();
                        Refresh();
                        break;
                case ("WaterCounter"):
                        WaterCounter waterCounter = CreateWaterCounter();
                        var ser2 = new Se.Backend.Services.ServiceFactory().CreateAddElementService(TxtB_route.Text);
                        TxtB_result.Text = ser2.AddElement(waterCounter).ToString();
                        Refresh();
                        break;
                default:
                    break;
                }
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
        }

        private WaterCounter CreateWaterCounter()
        {
            WaterCounter res = new WaterCounter();

            foreach (var p in res.GetType().GetProperties())
            {
                var w = new AddItem(p.ToString());
                if (w.ShowDialog() == true)
                {

                    var atr = Convert.ChangeType(w.res, p.PropertyType);
                    p.SetValue(res, atr);
                    Console.WriteLine(res);
                }

            }
            return res;
        }

        private ElectrycityMeter CreateElectrycityMeter()
        {
            ElectrycityMeter res = new ElectrycityMeter();

            foreach (var p in res.GetType().GetProperties())
            {
                var w = new AddItem(p.ToString());
                if (w.ShowDialog() == true)
                {
                    
                        var atr = Convert.ChangeType(w.res, p.PropertyType);
                        p.SetValue(res, atr);
                    Console.WriteLine(res);
                }

            }
            return res;
        }

        private Gateway CreateGateway()
        {
            Gateway gat = new Gateway();

            foreach (var p in gat.GetType().GetProperties())
            {
                var w = new AddItem(p.ToString());
                if (w.ShowDialog() == true)
                {
                    if (p.PropertyType == typeof(IPAddress))
                    {

                        var ip = IPAddress.Parse(w.res);
                        p.SetValue(gat, ip);

                    }
                    else
                    {
                        var atr = Convert.ChangeType(w.res, p.PropertyType);
                        p.SetValue(gat, atr);
                    }

                    Console.WriteLine(gat);
                }

            }
            return gat;
        }

        private void Btn_Refresh_Click(object sender, RoutedEventArgs e)
        {
            Refresh();
        }
    }
}
