﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Se.Backend.Services.Common;
using Se.Backend.Interfaces;
using Se.Backend.Entity;
using System.Data.SqlClient;
using System.Data;
using System.Xml.Linq;
using System.Net;
using System.IO;

namespace Se.Backend.Services
{
    public class DbService : BaseService, IDb
    {
        private XDocument _doc;
        private string _pathFile;
        private DbService _instance;

        
        public DbService(string pathFile)
        {
            _pathFile = pathFile;
            if (!File.Exists(pathFile))
            {
                try
                {
                    var myFile =  File.Create(_pathFile);
                    myFile.Close();

                    XDocument doc = new XDocument(new XElement("root"));
                    doc.Save(_pathFile);
                    _doc = XDocument.Load(_pathFile);



                }
                catch (Exception ex) { _logger.Error(ex.Message); }
            }
            else
            {

                _doc = XDocument.Load(_pathFile);
            }
        }

    
        public  DbService Instance (string pathFile) {
            if (_instance == null)
            {
                _instance = new DbService(pathFile);
            }
            
        return _instance;
    }
       
        public bool ExitsElectrycityMeter(ElectrycityMeter electrycityMeter)
        {
            bool res = false;
            foreach(var n in _doc.Descendants("ElectrycityMeter").Attributes("id"))
                {
                if (n.Value == electrycityMeter.Id) { res = true;break; }
            }
            return res;

        }

        public bool ExitsWaterCount(WaterCounter wc)
        {
            bool res = false;
            foreach (var n in _doc.Descendants("WaterCounter").Attributes("id"))
            {
                if (n.Value == wc.Id) { res = true; break; }
            }
            return res;

        }

        public bool ExitsGateway(Gateway g)
        {
            bool res = false;
            foreach (var n in _doc.Descendants("Gateway").Attributes("id"))
            {
                if (n.Value == g.Id) { res = true; break; }
            }
            return res;

        }
        public List<string>GetList()
        {
            var res =new List<string>();
            foreach (var n in _doc.Descendants("root"))
            {
                res.Add(n.ToString());
            }
            return res;
        }

        public EnumError Add(XElement e)
        {
            EnumError res = EnumError.NoError;
            try
            {
                _doc.Root.Add(e);
                Save();
            }
            catch { }
            return res;
        }

        
        private EnumError Save()
        {
            try
            {
                _doc.Save(_pathFile);
                return EnumError.NoError;
            }
            catch (Exception ex) { _logger.Error(ex.Message); return EnumError.Error; }
        }
    
    }
}