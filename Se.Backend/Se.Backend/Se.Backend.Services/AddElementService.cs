﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Se.Backend.Services.Common;
using Se.Backend.Interfaces;
using Se.Backend.Entity;
using System.Xml.Linq;
using System.Net;


namespace Se.Backend.Services

{
    public class AddElementService : BaseService, IAddElement
    {

        private string routeDb;
        DbService db;

        public AddElementService(string pathFile)
        {
            routeDb = pathFile;
            db = new DbService(routeDb);
        }
        /*
        public void Instance(string pathFile)
        {
            if (_instance == null)
            {

                _instance = new AddElementService(pathFile);

            }
            

            //return _instance;
        }
        */
        //String
        public EnumError AddElement(string element)
        {
            EnumError res = EnumError.NoError;
            XElement e = XElement.Load(element);
            switch (e.Name.LocalName)
            {
                case ("ElectrycityMeter"):

                    ElectrycityMeter eM = new ElectrycityMeter
                    {
                        Id = e.Attribute("id").Value,
                        Brand = e.Attribute("KeySerial").Value,
                        Model = e.Attribute("Model").Value,
                        KeySerial = e.Attribute("KeySerial").Value
                    };
                    AddElement(eM);

                    break;

                case ("WaterCounter"):
                    WaterCounter wC = new WaterCounter
                    {
                        Id = e.Attribute("id").Value,
                        Brand = e.Attribute("KeySerial").Value,
                        Model = e.Attribute("Model").Value,
                        KeySerial = e.Attribute("KeySerial").Value
                    };
                    AddElement(wC);
                    break;
                case ("Gateway"):
                    Gateway g = new Gateway
                    {
                        Id = e.Attribute("id").Value,
                        Brand = e.Attribute("KeySerial").Value,
                        Model = e.Attribute("Model").Value,
                        KeySerial = e.Attribute("KeySerial").Value,
                        Ip = IPAddress.Parse(e.Attribute("Ip").Value),
                        Port = Convert.ToUInt16(e.Attribute("Port").Value)


                    };
                    AddElement(g);
                    break;
                default:
                    res = EnumError.Error;
                    break;
            }
            return res;

        }
        ///WaterCount///
        public EnumError AddElement(WaterCounter wc)
        {
            
            if (!db.ExitsWaterCount(wc))
            {
                XElement eM = new XElement("WaterCounter",
                            new XAttribute("id", wc.Id),
                            new XAttribute("KeySerial", wc.KeySerial),
                            new XAttribute("Brand", wc.Brand),
                            new XAttribute("Model", wc.Model)
                           );
                db.Add(eM);
                return EnumError.NoError;
            }
            else { return EnumError.ElementExist; }
        }
        

        //Gateway
        public EnumError AddElement(Gateway g)
        {
            
            if (!db.ExitsGateway(g))
            {
                XElement eM = new XElement("Gateway",
                            new XAttribute("id", g.Id),
                            new XAttribute("KeySerial", g.KeySerial),
                            new XAttribute("Brand", g.Brand),
                            new XAttribute("Model", g.Model),
                            new XAttribute("Ip", g.Ip.ToString()),
                            new XAttribute("Port", g.Port)
                           );
                db.Add(eM);
                return EnumError.NoError;
            }
            else { return EnumError.ElementExist; }
        }

        //ElectrycityMeter////
        public EnumError AddElement(ElectrycityMeter electrycityMeter)
        {
          
            if (!db.ExitsElectrycityMeter(electrycityMeter))
            {
                XElement eM = new XElement("ElectrycityMeter",
                            new XAttribute("id", electrycityMeter.Id),
                            new XAttribute("KeySerial", electrycityMeter.KeySerial),
                            new XAttribute("Brand", electrycityMeter.Brand),
                            new XAttribute("Model", electrycityMeter.Model)
                           );
                db.Add(eM);
                
                return EnumError.NoError;
            }
            else { return EnumError.ElementExist; }
        }

        public EnumError AddElements(List<string> elements)
        {
            EnumError res = EnumError.NoError;
            foreach(string e in elements)
            {
                if (AddElement(e).Equals(EnumError.Error)) { res = EnumError.Error; }
            }
            return res;
        }

        public List<string> GetElements()
        {
           
            return db.GetList();
        }
    }
}
