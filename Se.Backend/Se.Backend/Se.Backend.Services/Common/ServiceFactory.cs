﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Se.Backend.Interfaces;
namespace Se.Backend.Services
{
    public class ServiceFactory : IServiceFactory
    {
        public IAddElement CreateAddElementService(string pathDb)
        {
            return new AddElementService(pathDb);
        }
        
    }
}
