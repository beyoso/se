﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Se.Backend.Entity;
using System.Xml.Linq;

namespace Se.Backend.Interfaces
{
    public interface IDb
    {

         

         bool ExitsElectrycityMeter(ElectrycityMeter electrycityMeter);

         bool ExitsWaterCount(WaterCounter wc);

         bool ExitsGateway(Gateway g);

         EnumError Add(XElement e);
    }
}
