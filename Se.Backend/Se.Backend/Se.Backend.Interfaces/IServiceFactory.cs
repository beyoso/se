﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Se.Backend.Interfaces
{
    public interface IServiceFactory
    {
        IAddElement CreateAddElementService(string pathDb);
    }
}
