﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Se.Backend.Entity;

namespace Se.Backend.Interfaces
{
    public interface IAddElement 
    {
        
        EnumError AddElement(ElectrycityMeter element);
        EnumError AddElement(Gateway gateway);
        EnumError AddElement(WaterCounter waterCounter);
        EnumError AddElement(string element);
        EnumError AddElements(List<string>elements);
        List<string> GetElements();

    }
}
