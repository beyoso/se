﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Se.Backend.Entity
{
    public class ElectrycityMeter
    {
        public string Id { get; set; }
        public string KeySerial { get; set; }
        //string or enum
        public string Brand { get; set; }
        //string or enum
        public string Model { get; set; }
    }
}
