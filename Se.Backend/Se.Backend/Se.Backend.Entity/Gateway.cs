﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;

namespace Se.Backend.Entity
{
    public class Gateway
    {
        public string Id { get; set; }
        public string KeySerial { get; set; }
        //string or enum
        public string Brand { get; set; }
        //string or enum
        public string Model { get; set; }
        public IPAddress Ip{ get; set; }
        //ushort because the ports go up to 65.535 and forever positive 
        public ushort Port { get; set; }
    }
}
