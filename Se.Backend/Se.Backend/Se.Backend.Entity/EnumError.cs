﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Se.Backend.Entity
{
    public enum EnumError
    {
        NoError = 0,
        Error = 1,
        ElementExist = 2
    }
}
